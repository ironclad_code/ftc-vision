package org.firstinspires.ftc.teamcode.Auto.Encoders;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.Auto.Recorder.EncoderRecorder;

//@TeleOp(name="Encoder_Backward", group="Encoder")
public class Encoder_Backwards extends LinearOpMode {

    DcMotor LF;
    DcMotor RF;
    DcMotor LR;
    DcMotor RR;
    DcMotor arm;
    EncoderRecorder recorder;

    @Override
    public void runOpMode() throws InterruptedException {
        LF = hardwareMap.get(DcMotor.class, "LF");
        RF = hardwareMap.get(DcMotor.class, "RF");
        LR = hardwareMap.get(DcMotor.class, "LR");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        recorder = new EncoderRecorder(LF,RF,LR,RR,arm, telemetry);
        waitForStart();
        if (opModeIsActive()) {
              while(true){
                        exampleRecordingCode:recorder.record(-0.1, 0.1, -0.1, 0.1,0, gamepad1.a);
                      }
            //recorder.run(5000, -5000, 5000, -5000, 1, 1);
           //recorder.run(5000, 5000, -5000, 5000, 1, 1);

        }
    }
}
