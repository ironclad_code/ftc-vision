package org.firstinspires.ftc.teamcode.Auto.Recorder;

import com.qualcomm.hardware.rev.RevBlinkinLedDriver;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class EncoderRecorder {

    DcMotor LF;
    DcMotor RF;
    DcMotor LR;
    DcMotor RR;
    DcMotor arm;
    boolean stopRecord;
    Telemetry telemetry;



    public EncoderRecorder(DcMotor LF, DcMotor RF, DcMotor LR, DcMotor RR, DcMotor arm, Telemetry telemetry) {


        this.LF = LF;
        this.RF = RF;
        this.LR = LR;
        this.RR = RR;
        this.arm = arm;
        this.telemetry = telemetry;
        reset();
    }



    public void run(int LF, int RF, int LR, int RR, int arm, double power, int accuracy) {
        int[] data = new int[5];
        reset();
        while (!isOnTarget(LF, RF, LR, RR, arm, accuracy)) {
            this.LF.setTargetPosition(LF);
            this.RF.setTargetPosition(RF);
            this.LR.setTargetPosition(LR);
            this.RR.setTargetPosition(RR);
            this.arm.setTargetPosition(arm);

            this.LF.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.RF.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.LR.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.RR.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.arm.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            this.LF.setPower(power);
            this.RF.setPower(power);
            this.LR.setPower(power);
            this.RR.setPower(power);
            this.arm.setPower(power);

            output(this.LF.getCurrentPosition(), this.RF.getCurrentPosition(), this.LR.getCurrentPosition(), this.RR.getCurrentPosition(), this.arm.getCurrentPosition());
        }
    }

    boolean recordFirstRun = false;
    //drive robot and record encoder pos
    public void record(double LF, double RF, double LR, double RR, double arm, boolean stop) throws InterruptedException {
        int[] data = new int[5];
        if(!recordFirstRun) {
            recordFirstRun = true;
            stopRecord = false;
            reset();
        }
        if(!stopRecord) {
            data[0] = runMotor(this.LF, LF);
            data[1] = runMotor(this.RF, RF);
            data[2] = runMotor(this.LR, LR);
            data[3] = runMotor(this.RR, RR);
            data[4] = runMotor(this.arm, arm);
            if(stop) stopRecord = true;
        } else {
            recordFirstRun = false;
            while(true) {
                data[0] = runMotor(this.LF, 0);
                data[1] = runMotor(this.RF, 0);
                data[2] = runMotor(this.LR, 0);
                data[3] = runMotor(this.RR, 0);
                data[4] = runMotor(this.arm, 0);
                Thread.sleep(50);
            }
        }
        output(data[0], data[1], data[2], data[3], data[4]);
    }

    //run a single motor and return encoder pos
    private int runMotor(DcMotor motor, double power) {
        motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motor.setPower(power);
        return motor.getCurrentPosition();
    }

    //reset all encoder pos
    public void reset() {
        LF.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        RF.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        LR.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        RR.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }

    private void output(int LF, int RF, int LR, int RR, int arm) { //i hate the telemetry class
        telemetry.addData("LF pos", LF);
        telemetry.addData("RF pos", RF);
        telemetry.addData("LR pos", LR);
        telemetry.addData("RR pos", RR);
        telemetry.addData("arm pos", arm);
        telemetry.update();
    }

    private boolean isOnTarget(int targ1, int targ2, int targ3, int targ4, int targ5, int accuracy) {
        int success = 0;
        if (Math.abs(this.LF.getCurrentPosition() - targ1) <= accuracy) success++;
        if (Math.abs(this.RF.getCurrentPosition() - targ2) <= accuracy) success++;
        if (Math.abs(this.LR.getCurrentPosition() - targ3) <= accuracy) success++;
        if (Math.abs(this.RR.getCurrentPosition() - targ4) <= accuracy) success++;
        if (Math.abs(this.arm.getCurrentPosition() - targ5) <= accuracy) success++;
        return success >= 2;
    }
}
