/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.Auto.Encoders;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Auto.Recorder.EncoderRecorder;

/**
 * This file illustrates the concept of driving a path based on time.
 * The code is structured as a LinearOpMode
 *
 * The code assumes that you do NOT have encoders on the wheels,
 *   otherwise you would use: RobotAutoDriveByEncoder;
 *
 *   The desired path in this example is:
 *   - Drive forward for 3 seconds
 *   - Spin right for 1.3 seconds
 *   - Drive Backward for 1 Second
 *
 *  The code is written in a simple form with no optimizations.
 *  However, there are several ways that this type of sequence could be streamlined,
 *
 * Use Android Studio to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this OpMode to the Driver Station OpMode list
 */

@Autonomous(name="Stok", group="Robot")
//@Disabled
public class Stok extends LinearOpMode {

    /* Declare OpMode members. */
    DcMotor LF;
    DcMotor RF;
    DcMotor LR;
    DcMotor RR;
    DcMotor arm;
    Servo leftClaw;
    Servo  rightClaw;
    EncoderRecorder recorder;

    private ElapsedTime     runtime = new ElapsedTime();


    static final double     FORWARD_SPEED = 0.6;
    static final double     TURN_SPEED    = 0.5;

    @Override
    public void runOpMode() {

        // Initialize the drive system variables.
        LF = hardwareMap.get(DcMotor.class, "LF");
        RF = hardwareMap.get(DcMotor.class, "RF");
        LR = hardwareMap.get(DcMotor.class, "LR");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        rightClaw = hardwareMap.get(Servo.class, "rightClaw");
        leftClaw = hardwareMap.get(Servo.class, "leftClaw");
        leftClaw.setDirection(Servo.Direction.REVERSE);
        recorder = new EncoderRecorder(LF,RF,LR,RR,arm, telemetry);

        // To drive forward, most robots need the motor on one side to be reversed, because the axles point in opposite directions.
        // When run, this OpMode should start both motors driving forward. So adjust these two lines based on your first test drive.
        // Note: The settings here assume direct drive on left and right wheels.  Gear Reduction or 90 Deg drives may require direction flips


        // Send telemetry message to signify robot waiting;
        telemetry.addData("Status", "Ready to run");    //
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // Step through each leg of the path, ensuring that the Auto mode has not been stopped along the way
opModeIsActive();
        recorder.run(-752, 752, -753, 751, 900, .5, 1);
        sleep(2000);
        rightClaw.setPosition(.35);
        leftClaw.setPosition(.35);
        sleep(2000);
        arm.setTargetPosition(2000);
        sleep(2000);
        recorder.run(902, -902, 903, -901, 500, .5, 1);
        recorder.run(452, 452, 453, 451, 0, .5, 1);
        recorder.run(-252, 252, -253, 251, -800, .5, 1);
        sleep(2000);
        rightClaw.setPosition(0);
        leftClaw.setPosition(0);
        //new rotation
        recorder.run(302, -302, 303, -301, -800, .5, 1);
        recorder.run(-452, -452, -453, -451, -500, .5, 1);
        recorder.run(-902, 902, -903, 901, -300, .5, 1);
        sleep(2000);
        rightClaw.setPosition(.35);
        leftClaw.setPosition(.35);
        sleep(2000);
        arm.setTargetPosition(2000);
        sleep(2000);
        recorder.run(902, -902, 903, -901, 500, .5, 1);
        recorder.run(452, 452, 453, 451, 0, .5, 1);
        recorder.run(-302, 302, -303, 301, -800, .5, 1);
        sleep(2000);
        rightClaw.setPosition(0);
        leftClaw.setPosition(0);
        //new new rotation
        recorder.run(302, -302, 303, -301, -800, .5, 1);
        recorder.run(-452, -452, -453, -451, -500, .5, 1);
        recorder.run(-902, 902, -903, 901, -300, .5, 1);
        sleep(2000);
        rightClaw.setPosition(.35);
        leftClaw.setPosition(.35);
        sleep(2000);
        arm.setTargetPosition(2000);
        sleep(2000);
        recorder.run(902, -902, 903, -901, 500, .5, 1);
        recorder.run(452, 452, 453, 451, 0, .5, 1);
        recorder.run(-302, 302, -303, 301, -800, .5, 1);
        sleep(2000);
        rightClaw.setPosition(0);
        leftClaw.setPosition(0);
        telemetry.update();
        stop();
        sleep(2000);
    }
}
