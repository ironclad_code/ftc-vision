package org.firstinspires.ftc.teamcode.TenserFlow;

import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

public class TfUtils {

    public VuforiaLocalizer initVuforia(String key, VuforiaLocalizer.CameraDirection direction) {
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();
        parameters.vuforiaLicenseKey = key;
        parameters.cameraDirection = direction;
        VuforiaLocalizer vuforia = ClassFactory.getInstance().createVuforia(parameters);
        return vuforia;
    }

    public TFObjectDetector initTfod(String assetName, String[] labels, float minResultConfidence, boolean isModelTensorFLow2, int inputSize, HardwareMap hardwareMap, VuforiaLocalizer vuforia) {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfodParameters.minResultConfidence = minResultConfidence;
        tfodParameters.isModelTensorFlow2 = isModelTensorFLow2;
        tfodParameters.inputSize = inputSize;
        TFObjectDetector tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(assetName, labels);
        return tfod;
    }
}
