

package org.firstinspires.ftc.teamcode.Auto.Encoders;

import android.annotation.SuppressLint;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.Auto.Recorder.EncoderRecorder;
import org.firstinspires.ftc.teamcode.pipelines.AprilTagDetectionPipeline;
import org.openftc.apriltag.AprilTagDetection;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;

import java.util.ArrayList;

@Autonomous(name="Left")
public class Left extends LinearOpMode
{
    OpenCvCamera camera;
    AprilTagDetectionPipeline aprilTagDetectionPipeline;



    DcMotor LF;
    DcMotor RF;
    DcMotor LR;
    DcMotor RR;
    DcMotor arm;
    Servo leftClaw;
    Servo  rightClaw;
    EncoderRecorder recorder;



    private ElapsedTime runtime = new ElapsedTime();

    static final double FEET_PER_METER = 3.28084;

    // Lens intrinsics
    // UNITS ARE PIXELS
    // NOTE: this calibration is for the C920 webcam at 800x448.
    // You will need to do your own calibration for other configurations!
    double fx = 578.272;
    double fy = 578.272;
    double cx = 402.145;
    double cy = 221.506;

    // UNITS ARE METERS
    double tagsize = 0.166;

    // Tag ID 1,2,3 from the 36h11 family
    int LEFT = 1;
    int MIDDLE = 2;
    int RIGHT = 3;

    AprilTagDetection tagOfInterest = null;

    @Override
    public void runOpMode()
    {



        LF = hardwareMap.get(DcMotor.class, "LF");
        RF = hardwareMap.get(DcMotor.class, "RF");
        LR = hardwareMap.get(DcMotor.class, "LR");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        rightClaw = hardwareMap.get(Servo.class, "rightClaw");
        leftClaw = hardwareMap.get(Servo.class, "leftClaw");
        leftClaw.setDirection(Servo.Direction.REVERSE);
        recorder = new EncoderRecorder(LF,RF,LR,RR,arm, telemetry);

        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        camera = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class, "Webcam 1"), cameraMonitorViewId);
        aprilTagDetectionPipeline = new AprilTagDetectionPipeline(tagsize, fx, fy, cx, cy);

        camera.setPipeline(aprilTagDetectionPipeline);
        camera.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener()
        {
            @Override
            public void onOpened()
            {
                camera.startStreaming(800,448, OpenCvCameraRotation.UPRIGHT);
            }

            @Override
            public void onError(int errorCode)
            {

            }
        });

        telemetry.setMsTransmissionInterval(50);

        /*
         * The INIT-loop:
         * This REPLACES waitForStart!
         */
        while (!isStarted() && !isStopRequested())
        {
            ArrayList<AprilTagDetection> currentDetections = aprilTagDetectionPipeline.getLatestDetections();

            if(currentDetections.size() != 0)
            {
                boolean tagFound = false;

                for(AprilTagDetection tag : currentDetections)
                {
                    if(tag.id == LEFT || tag.id == MIDDLE || tag.id == RIGHT)
                    {
                        tagOfInterest = tag;
                        tagFound = true;
                        break;
                    }
                }

                if(tagFound)
                {
                    telemetry.addLine("Tag of interest is in sight!\n\nLocation data:");
                    tagToTelemetry(tagOfInterest);
                }
                else
                {
                    telemetry.addLine("Don't see tag of interest :(");

                    if(tagOfInterest == null)
                    {
                        telemetry.addLine("(The tag has never been seen)");
                    }
                    else
                    {
                        telemetry.addLine("\nBut we HAVE seen the tag before; last seen at:");
                        tagToTelemetry(tagOfInterest);
                    }
                }

            }
            else
            {
                telemetry.addLine("Don't see tag of interest :(");

                if(tagOfInterest == null)
                {
                    telemetry.addLine("(The tag has never been seen)");
                }
                else
                {
                    telemetry.addLine("\nBut we HAVE seen the tag before; last seen at:");
                    tagToTelemetry(tagOfInterest);
                }

            }

            telemetry.update();
            sleep(20);
        }

        /*
         * The START command just came in: now work off the latest snapshot acquired
         * during the init loop.
         */

        /* Update the telemetry */
        if(tagOfInterest != null)
        {
            telemetry.addLine("Tag snapshot:\n");
            tagToTelemetry(tagOfInterest);
            telemetry.update();
        }
        else
        {
            telemetry.addLine("No tag snapshot available, it was never sighted during the init loop :(");
            telemetry.update();
        }

        /* Actually do something useful */
        if(tagOfInterest == null){
            //default trajectory here if preferred
            //tag1
        }else if(tagOfInterest.id == LEFT){
            rightClaw.setPosition(.35);
            leftClaw.setPosition(.35);
            sleep(2000);
            recorder.run(-992, 982, -983, 981, 800, .5, 1);
            recorder.run(-1416, -1404, 1375, 1404, 1400, .5, 1);
            recorder.run(-99, 98, -98, 98, 100, .5, 1);
            recorder.run(-772, -772, -773, -771, 800, .5, 1);
            recorder.run(-316, -313, -316, -318, 0, .5, 1);
            recorder.run(-384, 388, -385, 389, 0, .5, 1);
            recorder.run(0, 0, 0, 0, -300, .5, 1);
            sleep(2000);
            rightClaw.setPosition(0);
            leftClaw.setPosition(0);
            sleep(2000);
            recorder.run(419, -401, 418, -412, 0, .5, 1);
            recorder.run(355, 347, 342, 345, 0, .5, 1);
            recorder.run(-586, 585, -579, 579, 0, .5, 1);
            //tag2
        }else if(tagOfInterest.id == MIDDLE){
            rightClaw.setPosition(.35);
            leftClaw.setPosition(.35);
            sleep(2000);
            recorder.run(-992, 982, -983, 981, 800, .5, 1);
            recorder.run(-1416, -1404, 1375, 1404, 1400, .5, 1);
            recorder.run(-99, 98, -98, 98, 100, .5, 1);
            recorder.run(-772, -772, -773, -771, 800, .5, 1);
            recorder.run(-316, -313, -316, -318, 0, .5, 1);
            recorder.run(-374, 378, -375, 379, 0, .5, 1);
            recorder.run(0, 0, 0, 0, -300, .5, 1);
            sleep(2000);
            rightClaw.setPosition(0);
            leftClaw.setPosition(0);
            sleep(2000);
            recorder.run(-890, -868, 862, 869, 0, .5, 1);
            recorder.run(-302, 293, -299, 292, 0, .5, 1);
            recorder.run(338, 320, 315, 311, 0, .5, 1);
            //tag3
        }else if(tagOfInterest.id == RIGHT){
            rightClaw.setPosition(.35);
            leftClaw.setPosition(.35);
            sleep(2000);
            recorder.run(-992, 982, -983, 981, 800, .5, 1);
            recorder.run(-1416, -1404, 1375, 1404, 1400, .5, 1);
            recorder.run(-99, 98, -98, 98, 100, .5, 1);
            recorder.run(-772, -772, -773, -771, 800, .5, 1);
            recorder.run(-316, -313, -316, -318, 0, .5, 1);
            recorder.run(-354, 358, -355, 359, 0, .5, 1);
            recorder.run(0, 0, 0, 0, -300, .5, 1);
            sleep(2000);
            rightClaw.setPosition(0);
            leftClaw.setPosition(0);
            sleep(2000);
            recorder.run(-890, -868, 862, 869, 0, .5, 1);
            recorder.run(-302, 293, -299, 292, 0, .5, 1);
            recorder.run(338, 320, 315, 311, 0, .5, 1);
            recorder.run(-1281, -1263, 1263, 1264, 0, .5, 1);
        }


        /* You wouldn't have this in your autonomous, this is just to prevent the sample from ending */
        while (opModeIsActive()) {sleep(20);}
    }

    @SuppressLint("DefaultLocale")
    void tagToTelemetry(AprilTagDetection detection)
    {
        telemetry.addLine(String.format("\nDetected tag ID=%d", detection.id));
        telemetry.addLine(String.format("Translation X: %.2f feet", detection.pose.x*FEET_PER_METER));
        telemetry.addLine(String.format("Translation Y: %.2f feet", detection.pose.y*FEET_PER_METER));
        telemetry.addLine(String.format("Translation Z: %.2f feet", detection.pose.z*FEET_PER_METER));
        telemetry.addLine(String.format("Rotation Yaw: %.2f degrees", Math.toDegrees(detection.pose.yaw)));
        telemetry.addLine(String.format("Rotation Pitch: %.2f degrees", Math.toDegrees(detection.pose.pitch)));
        telemetry.addLine(String.format("Rotation Roll: %.2f degrees", Math.toDegrees(detection.pose.roll)));
    }
}