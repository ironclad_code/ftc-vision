package org.firstinspires.ftc.teamcode.TenserFlow;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

//@TeleOp
public class TenserFlow extends LinearOpMode {

    private static final String VUFORIA_KEY = "AejuXyz/////AAABmV5PsqDnJE10mtR5VxkLfcVMuXWj4KTdLf61If6aFX7yzB9ktBkZle3lmMgWVQkL75lGHW5MyNbVB9mIWVVlrItRshUxx0AHUacpscngbgvykjPjMPmidgtKk8vNnUTdpEvVVWwqViHM/fcTHW0tMzWTO5etisQ5boT8JrFLZVeB+aHxThj62+aEYOHtrRBOUgajfZxL+NYtQWRmBzw9KMQdO4rPvhbXfb0mEUJl43WgG5+xDvFeABh0IcpcwYQQzm6yCCAjUq407LXXjplXeYGpSsKF8LZOG/iFK3JKlHjwmKTzySmC5Qen32X1UvWUg4yliZR6A0Zy/1Pog13GDHskRKCuU0FrXT0957JFfkMM";
    private TfUtils tfUtils = new TfUtils();
    private static final String TFOD_MODEL_ASSET = "PowerPlay.tflite";
    private static final String[] LABELS = {
            "Bolt",
            "Bulb",
            "Panel"
    };

    @Override
    public void runOpMode() throws InterruptedException {
        VuforiaLocalizer vuforia = tfUtils.initVuforia(VUFORIA_KEY, VuforiaLocalizer.CameraDirection.BACK);
        TFObjectDetector tfod = tfUtils.initTfod(TFOD_MODEL_ASSET, LABELS, 0.8f, true, 420, hardwareMap, vuforia);
        if(tfod != null) {
            tfod.activate();
            tfod.setZoom(2.5, 16.0/9.0);
        }
        waitForStart();
        while(opModeIsActive()) {
            List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
            if(updatedRecognitions != null) {
                telemetry.addData("# Object Detected", updatedRecognitions.size());
                if(updatedRecognitions.size() != 0) {
                    for(int i = 0; i < updatedRecognitions.size(); i++) {
                        telemetry.addData(String.format("label (%d)", i), updatedRecognitions.get(i).getLabel());
                        telemetry.addData(String.format("  left,top (%d)", i), "%.03f , %.03f", updatedRecognitions.get(i).getLeft(), updatedRecognitions.get(i).getTop());
                        telemetry.addData(String.format("  right,bottom (%d)", i), "%.03f , %.03f", updatedRecognitions.get(i).getRight(), updatedRecognitions.get(i).getBottom());
                    }
                }
                telemetry.update();
            }
        }
    }
}