package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.stream.CameraStreamServer;
import org.firstinspires.ftc.teamcode.pipelines.AprilTagDetectionPipeline;
import org.openftc.apriltag.AprilTagDetection;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvWebcam;

import java.util.ArrayList;

//@Autonomous
public class AprilTag<tagToTelemetry> extends LinearOpMode {
    OpenCvWebcam webcam;
    AprilTagDetectionPipeline pipeline;
    static final double FEET_PER_METER = 3.28084;

    // Lens intrinsics
    // UNITS ARE PIXELS
    // NOTE: this calibration is for the C920 webcam at 800x448.
    // You will need to do your own calibration for other configurations!
    double fx = 578.272;
    double fy = 578.272;
    double cx = 402.145;
    double cy = 221.506;

    // UNITS ARE METERS
    double tagsize = 0.166;

    int LEFT = 1;
    int MIDDLE = 2;
    int RIGHT = 3;

    AprilTagDetection tagOfInterest = null;

    @Override
    public void runOpMode() throws InterruptedException {
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        webcam = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class, "Webcam"), cameraMonitorViewId);
        pipeline = new AprilTagDetectionPipeline(tagsize, fx, fy, cx, cy);

        webcam.setPipeline(pipeline);
        webcam.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener() {
            @Override
            public void onOpened() {
                webcam.startStreaming(800,448, OpenCvCameraRotation.UPRIGHT);
            }

            @Override
            public void onError(int errorCode) {

            }
        });
        CameraStreamServer.getInstance().setSource(webcam);
        waitForStart();
        while(opModeIsActive()) {
            ArrayList<AprilTagDetection> detections = pipeline.getLatestDetections();
            if (detections.size() != 0) {
                boolean tagFound = false;
                for (AprilTagDetection tag : detections) {
                    telemetry.addData("tag id", tag.id);
                    telemetry.addData("x", Math.toDegrees(tag.pose.yaw));
                    if(tag.id == LEFT || tag.id == MIDDLE || tag.id ==RIGHT){
                        tagOfInterest = tag;
                        tagFound = true;
                        break;
                    }
                }
                if(tagFound)
                {
                    telemetry.addLine("Tag of interest is in sight! \n\n Location data: ");
                    tagToTelemetry(tagOfInterest);
                }
                else
                {
                    telemetry.addLine("Don't see the tag of interest:(");
                    if(tagOfInterest == null)
                    {
                        telemetry.addLine("(The tag has never been seen)");
                    }
                    else
                    {
                        telemetry.addLine("\n But we have seen the tag before; last seen at:");
                        tagToTelemetry(tagOfInterest);
                    }
                }
            }
            else
            {
                telemetry.addLine("Don't see the tag of interest :(");
                if(tagOfInterest == null)
                {
                    telemetry.addLine("(The tag has never been seen)");
                }
                else
                {
                    telemetry.addLine("\n But we have seen the tag before; last seen at:");
                    tagToTelemetry(tagOfInterest);
                }
                if(tagOfInterest != null)
                {
                    telemetry.addLine("Tag snapshot:\n");
                    tagToTelemetry(tagOfInterest);
                    telemetry.update();
                }
                else
                {
                    telemetry.addLine("No tag snapshot available, it was never sighted during the init loop :(");
                    telemetry.update();
                }
                if(tagOfInterest == null|| tagOfInterest.id == LEFT)
                {}
                else if(tagOfInterest.id == MIDDLE)
                {}
                else{}
                while(opModeIsActive()) {//sleep(20);
                //add auto code here probably

                }
            }
            /*tagToTelemetry Object;
            Object detection = null;
            (AprilTagDetection)
            {
                telemetry.addLine(String.format("\n Detected tag ID =%d", detection.id)),
                //telemetry.addLine(String.format("Translation X: %.2f feet",detection.pose.x*FEET_PER_METER));
                //telemetry.addLine(String.format("Translation Y: %.2f feet",detection.pose.y*FEET_PER_METER));
               // telemetry.addLine(String.format("Translation Z: %.2f feet",detection.pose.z*FEET_PER_METER));
                //telemetry.addLine(String.format("Rotation Yaw %.2f degrees", Math.toDegrees(detection.pose.yaw)));
                //telemetry.addLine(String.format("Rotation Pitch %.2f degrees", Math.toDegrees(detection.pose.pitch));
                //telemetry.addLine(String.format("Rotation Roll %.2f degrees", Math.toDegrees(detection.pose.roll));
            }*/
            telemetry.update();
            sleep(100);
        }
    }

    private void tagToTelemetry(AprilTagDetection tagOfInterest) {
    }
}
