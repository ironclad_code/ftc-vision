package org.firstinspires.ftc.teamcode.Auto.Encoders;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Auto.Recorder.EncoderRecorder;

//@Autonomous(name="Encoder_Auto_BL", group="Encoder")
public class Encoder_Auto_BL extends LinearOpMode {

    DcMotor LF;
    DcMotor RF;
    DcMotor LR;
    DcMotor RR;
    DcMotor arm;
    Servo leftClaw;
    Servo  rightClaw;
    EncoderRecorder recorder;

    private final ElapsedTime runtime = new ElapsedTime();

    @Override
    public void runOpMode() throws InterruptedException {
        LF = hardwareMap.get(DcMotor.class, "LF");
        RF = hardwareMap.get(DcMotor.class, "RF");
        LR = hardwareMap.get(DcMotor.class, "LR");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        rightClaw = hardwareMap.get(Servo.class, "rightClaw");
        leftClaw = hardwareMap.get(Servo.class, "leftClaw");
        rightClaw.setDirection(Servo.Direction.REVERSE);
        recorder = new EncoderRecorder(LF,RF,LR,RR,arm, telemetry);

        waitForStart();
        if (opModeIsActive()) {
             /* while(true){
                        exampleRecordingCode:recorder.record(0.1, -0.1, 0.1, -0.1,0, gamepad1.a);
                      }
                      */
            leftClaw.setPosition(0);
            rightClaw.setPosition(0);
            runtime.reset();
            while (opModeIsActive() && (runtime.seconds() < 1.0)) {
                telemetry.addData("Path", "Leg 1: %4.1f S Elapsed", runtime.seconds());
                telemetry.update();
            }


            recorder.run(1002, 972, -935, -981, 892, .5, 1);
            leftClaw.setPosition(0);
            rightClaw.setPosition(0);
        }
    }
}
